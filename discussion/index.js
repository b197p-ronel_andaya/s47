const fullNameElement = document.querySelector('#full-name');
const firstNameElement = document.querySelector('#first-name');
const lastNameElement = document.querySelector('#last-name');

let firstName = '';
let lastName = ''


function setFirstNameValue(){
    firstName = firstNameElement.value;
    fullNameElement.innerHTML = `${firstName} ${lastName}`;
}

function setLastNameValue(){
    lastName = lastNameElement.value;
    fullNameElement.innerHTML = `${firstName} ${lastName}`;
}

firstNameElement.addEventListener('keyup', setFirstNameValue);
lastNameElement.addEventListener('keyup', setLastNameValue);

